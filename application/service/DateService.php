<?php

declare(strict_types=1);

namespace app\application\service;

class DateService
{
    public static function getDaysBetweenTodayAndDate(string $date): float
    {
        $now = time();
        $your_date = strtotime($date);
        $datediff = $now - $your_date;

        return floor($datediff / (60 * 60 * 24));
    }
}
