<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <h1>Potwierdzenie utworzenia konta</h1>
    <p>Zostało utworzone nowe konto w naszym systemie dla adresu email:<br>
        Email: <?= $this->params['email'] ?> <br>
        Hasło: <?= $this->params['password'] ?> <br>
    </p>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
