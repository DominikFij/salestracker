<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ChangePasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Zmiana hasła';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-change-pass">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Proszę wypełnić pola aby zmienić hasło</p>

    <?php $form = ActiveForm::begin([
        'id' => 'change-pass-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'confirmPassword')->passwordInput() ?>


    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'change-pass-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
