<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Lista użytkowników';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Imię</th>
            <th scope="col">Nazwisko</th>
            <th scope="col">Email</th>
            <th scope="col">Data urodzenia</th>
            <th scope="col">Data utworzenia konta</th>
        </tr>
        </thead>
        <tbody>

        <?php
        foreach ($users as $user) {
            ?>

            <tr>
                <td><?= $user['firstname'] ?></td>
                <td><?= $user['lastname'] ?></td>
                <td><?= $user['email'] ?></td>
                <td><?= $user['birthDate'] ?></td>
                <td><?= $user['createdAt'] ?></td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>
</div>
