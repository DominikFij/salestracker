<?php

/* @var $this yii\web\View */


use yii\helpers\Html;


$this->title = 'Wynik importu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <h3>Dodani użytkownicy</h3>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Email</th>
        </tr>
        </thead>
        <tbody>

        <?php
        foreach ($addedUsers as $userEmail) {
            ?>

            <tr>
                <td><?= $userEmail ?></td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>

    <h3>Instniejący użytkownicy</h3>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Email</th>
        </tr>
        </thead>
        <tbody>

        <?php
        foreach ($existsUsers as $userEmail) {
            ?>

            <tr>
                <td><?= $userEmail ?></td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>
</div>
