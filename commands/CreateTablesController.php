<?php

declare(strict_types=1);

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Exception;

class CreateTablesController extends Controller
{
    /**
     * @return int Exit code
     */
    public function actionIndex()
    {
        try {
            Yii::$app->db->createCommand()->createTable('user', [
                'id' => 'pk',
                'firstname' => 'string',
                'lastname' => 'string',
                'email' => 'string unique',
                'password' => 'string',
                'authKey' => 'string',
                'accessToken' => 'string',
                'birthDate' => 'date',
                'createdAt' => 'datetime',
                'passChangedAt' => 'datetime'
            ])->execute();

            Yii::$app->db->createCommand()->createIndex('uniqueEmail', 'user', 'email', true);

            Yii::$app->db->createCommand()->insert('user', [
                'firstname' => 'Dominik',
                'lastname' => 'Fijałek',
                'email' => 'abc123@wp.pl',
                'password' => Yii::$app->getSecurity()->generatePasswordHash('abc123'),
                'authKey' => 'authKey',
                'accessToken' => 'token',
                'birthDate' => '1996-06-30',
                'createdAt' => (new \DateTime())->format('Y-m-d H:i:s'),
                'passChangedAt' => (new \DateTime())->format('Y-m-d H:i:s')
            ])->execute();
        } catch (Exception $e) {
        }

        return ExitCode::OK;
    }
}
