<?php

namespace app\controllers;

use app\application\service\DateService;
use app\models\ChangePasswordForm;
use app\models\FileForm;
use app\models\User;
use Yii;
use yii\db\IntegrityException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     *
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            $model = new FileForm();
            if ($model->load(Yii::$app->request->post())) {
                $file = UploadedFile::getInstance($model, 'file');

                if ($file) {
                    $filename = '/app/upload/' . $file->name;

                    $file->saveAs($filename);

                    if (in_array($file->extension, array('xls', 'xlsx'))) {
                        $addedUsers = [];
                        $existsUsers = [];

                        $fileType = \PHPExcel_IOFactory:: identify($filename);
                        $excelReader = \PHPExcel_IOFactory::createReader($fileType);

                        $phpexcel = $excelReader->load($filename)->getsheet(0);
                        $totalLine = $phpexcel->gethighestrow();
                        $totalColumn = $phpexcel->gethighestcolumn();

                        if (1 < $totalLine) {
                            for ($row = 2; $row <= $totalLine; $row++) {
                                $data = [];
                                for ($column = 'A'; $column <= $totalColumn; $column++) {

                                    if (\PHPExcel_Shared_Date::isDateTime($phpexcel->getCell($column . $row))) {
                                        $date = \PHPExcel_Shared_Date::ExcelToPHPObject(trim($phpexcel->getCell($column . $row)));
                                        $data[] = trim($date->format('Y-m-d'));
                                    } else {
                                        $data[] = trim($phpexcel->getCell($column . $row));
                                    }
                                }
                                $password = User::generatePass(8);
                                $passwordHash = Yii::$app->getSecurity()->generatePasswordHash($password);
                                try {
                                    User::addUser($data[0], $data[1], $data[2], $passwordHash, '', '', $data[3]);
                                    $addedUsers[] = $data[2];

                                    Yii::$app->mailer->view->params['email'] = $data[2];
                                    Yii::$app->mailer->view->params['password'] = $password;
                                    $fromEmail = Yii::$app->params['senderEmail'];
                                    Yii::$app->mailer->compose('layouts/html.php')
                                        ->setFrom($fromEmail)
                                        ->setTo($data[2])
                                        ->setSubject('Utworzenie konta')
                                        ->send();
                                } catch (IntegrityException $ex) {
                                    $existsUsers[] = $data[2];
                                }
                            }
                        }

                        return $this->render('importResult', [
                            'addedUsers' => $addedUsers,
                            'existsUsers' => $existsUsers
                        ]);
                    }
                }

                return $this->render('fileUpload', ['model' => $model]);
            }

            return $this->render('fileUpload', [
                'model' => $model
            ]);
        }

        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $changePasswordDays = 30;

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $days = DateService::getDaysBetweenTodayAndDate($model->getUser()->getPassChangesAt());
            if($days > $changePasswordDays) {
                return Yii::$app->response->redirect(['site/change', 'email' => $model->getUser()->getEmail()]);
            }

            if($model->login()) {
                return $this->goBack();
            }
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays list page.
     *
     * @return Response|string
     */
    public function actionList()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $users = User::getUsers();

        return $this->render('list', [
            'users' => $users
        ]);
    }

    /**
     * Displays list page.
     *
     * @return Response|string
     */
    public function actionChange()
    {
        $model = new ChangePasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->validatePasswords('confirmPassword')) {
            $email = Yii::$app->request->getQueryParam('email');

            if($email !== '') {
                $passwordHash = Yii::$app->getSecurity()->generatePasswordHash($model->getPassword());
                User::updatePassword($email, $passwordHash);
                return Yii::$app->response->redirect(['site/login']);
            }
        }

        return $this->render('changePassword', [
            'model' => $model,
        ]);
    }
}
