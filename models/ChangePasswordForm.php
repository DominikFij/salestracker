<?php

declare(strict_types=1);

namespace app\models;

use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $password;
    public $confirmPassword;


    /**
     * @return array the validation rules.
     */
    public function rules(): array
    {
        return [
            [['password', 'confirmPassword'], 'required'],
            [['password', 'confirmPassword'], 'validatePasswords']
        ];
    }

    /**
     * Check if passwords are same
     *
     * @return bool
     */
    public function validatePasswords(string $attribute): bool
    {
        if($this->password === $this->confirmPassword) {
            return true;
        } else {
            $this->addError($attribute, 'Passwords are not the same.');

            return false;
        }
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
