<?php

declare(strict_types=1);

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class FileForm extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'],'file', 'skipOnEmpty' => false,'extensions' => 'xls,xlsx'],
        ];
    }

    public function attributeLabels()
    {
        return ['file'=>'upload file'];
  }

    public function upload()
    {
        var_dump('aaa');
        $file = UploadedFile::getInstance($this, 'file');

        if ($this->rules()) {
            $tmp_file = $file->baseName . '.' . $file->extension;
            $path = 'upload/';
            $dirpath = dirname(getcwd());
            if (is_dir($path)) {
                $file->saveAs($path . $tmp_file);
            } else {
                mkdir($path, 0777, true);
            }
            $file->saveAs($path . $tmp_file);
            return true;
        } else {
            Return 'validation failed';
        }
    }
}
