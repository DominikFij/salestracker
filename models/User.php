<?php

namespace app\models;

use Yii;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $password;
    public $authKey;
    public $accessToken;
    public $birthDate;
    public $createdAt;
    public $passChangedAt;


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id): ?self
    {
        $user = Yii::$app->db->createCommand('SELECT * FROM user WHERE id = :id')
            ->bindValue(':id', $id)
            ->queryOne();

        return $user !== null ? new static($user) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null): ?self
    {
        $user = Yii::$app->db->createCommand('SELECT * FROM user WHERE accessToken = :token')
            ->bindValue(':token', $token)
            ->queryOne();

        return $user !== null ? new static($user) : null;
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail(string $email): ?self
    {
        $user = Yii::$app->db->createCommand('SELECT * FROM user WHERE email = :email')
            ->bindValue(':email', $email)
            ->queryOne();

        return $user !== null ? new static($user) : null;
    }

    public static function addUser(string $firstname, string $lastname, string $email,
                                 string $password, string $authKey, string $accessToken, string $birthDate
    ): void {
        Yii::$app->db->createCommand('INSERT INTO user
                                (firstname, lastname, email, 
                                 password, authKey, accessToken, 
                                 birthDate, createdAt, passChangedAt)
                                 VALUES 
                                 (:firstname, :lastname, :email, 
                                 :password, :authKey, :accessToken, 
                                 :birthDate, NOW(), NOW())')
            ->bindValue(':firstname', $firstname)
            ->bindValue(':lastname', $lastname)
            ->bindValue(':email', $email)
            ->bindValue(':password', $password)
            ->bindValue(':authKey', $authKey)
            ->bindValue(':accessToken', $accessToken)
            ->bindValue(':birthDate', $birthDate)
            ->execute();
    }

    public static function getUsers(): array
    {
        $users = Yii::$app->db->createCommand('SELECT firstname, lastname, email, birthDate, createdAt FROM user')
            ->queryAll();

        return $users;
    }

    public static function updatePassword(string $email, string $password): void
    {
        Yii::$app->db->createCommand('UPDATE user SET 
                                            password=:password, passChangedAt=NOW()
                                            WHERE email = :email
                                ')
            ->bindValue(':password', $password)
            ->bindValue(':email', $email)
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function getPassChangesAt(): string
    {
        return $this->passChangedAt;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Validates password
     *
     * @param string $email email to validate
     * @return bool if password provided is valid for current user
     */
    public function validateEmail(string $email)
    {
        return $this->email === $email;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password, $hash)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $hash);
    }

    public static function generatePass($length)
    {
        $signs = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*';
        $pass = array();
        $alphaLength = strlen($signs) - 1;
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $signs[$n];
        }
        return implode($pass);
    }
}
